<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<%@ page contentType="text/html; charset=UTF-8" %>
	<title>Zadanie zaliczeniowe 1</title>
	</head>
	<body>
		<h1> Formularz do wyliczania rat kredytu: </h1>
		 <form action="credit" method="post">
			  Wartość kredytu: 
			  <input type="number" name="creditValue"  min=1 required><br><br>
			  Liczba rat w miesiącach:
			  <input type="number" name="numberOfRates" min=1 required><br><br>
			  Oprocentowanie:
			  <input type="number" name="interest" min=0 required> % <br><br>
			  Opłata stała:
			  <input type="number" name="constantCharge" min=0 required><br><br>
			  Rodzaj rat:
			  <input type="radio" name="kindOfRate" value="decreasing" checked>Malejąca
			  <input type="radio" name="kindOfRate" value="constant">Stała<br>
			  <button type="submit">Wylicz</button>
			  <button type="submit" class='btn-export' name='export'>Export to PDF</button>
		</form> 
	</body>
</html>