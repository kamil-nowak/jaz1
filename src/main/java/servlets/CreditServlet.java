package servlets;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Credit;
import model.SiteSettings;

@WebServlet("/credit")
public class CreditServlet extends HttpServlet {
	private String COL1 = "Nr raty";
	private String COL2 = "Kwota Kapitalu";
	private String COL3 = "Kwota odsetek";
	private String COL4 = "Oplaty stale";
	private String COL5 = "Calkowita kwota raty";

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Credit credit = new Credit(
				Integer.parseInt(request.getParameter("creditValue")),
				Integer.parseInt(request.getParameter("numberOfRates")),
				Double.parseDouble(request.getParameter("interest")),
				Integer.parseInt(request.getParameter("constantCharge")),
				request.getParameter("kindOfRate")
				);
		
		ArrayList<String> columns = new ArrayList<String>(Arrays.asList(COL1, COL2, COL3, COL4, COL5));
		SiteSettings site = new SiteSettings();
		StringBuilder table = createTable(credit, columns);
		site.setTitle("Zadanie zaliczeniowe 1");
		site.setHead("<style> th, td {padding: 5px;text-align: left; border: 0.5px solid black;}</style>");
		site.setBody("<h1>Harmonogram splat</h1>");	
		site.appendToBody(table);
		response.setContentType("text/html;charset=UTF-8");		
		if (request.getParameter("export") != null) {	
			Reader reader = new StringReader(site.getPage().toString());
			utils.Helpers.exportToPDF(request, response, reader);
		} else {
			response.getWriter().print(site.getPage());
		}
	}

	private StringBuilder createTable(Credit credit, ArrayList<String> columns) {
		double creditValue = credit.getCreditValue();
		int numberOfRates = credit.getNumberOfRates();
		double interest = credit.getInterest();
		double constantCharge = credit.getConstantCharge();
		
		StringBuilder sb = new StringBuilder();		
		sb.append("<table style='border: 1px solid black;'>");
		sb.append(createRow(columns));
		
		for (int rateNumber = 1; rateNumber <= numberOfRates; rateNumber++) {
			double amountOfInterest = utils.Helpers.roundTo2Dec(creditValue * interest / 12);
			ArrayList<Number> listOfValues = new ArrayList<Number>(Arrays.asList(rateNumber, creditValue, amountOfInterest,
					constantCharge, credit.getRate(rateNumber)));
			sb.append(createRow(listOfValues));
			creditValue -= credit.getRate(rateNumber);
			creditValue = utils.Helpers.roundTo2Dec(creditValue);
		}
		
		sb.append("</table>");
		return sb;
	}

	public <T> String createRow(ArrayList<T> mylist) {
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>");
		for (T value : mylist) {
			sb.append("<td>");
			sb.append(value);
			sb.append("</td>");
		}
		sb.append("</tr>");
		return sb.toString();
	}
}
