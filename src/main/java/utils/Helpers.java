package utils;

import java.io.IOException;
import java.io.Reader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class Helpers {

	public static void exportToPDF(HttpServletRequest request, HttpServletResponse response, Reader reader)
			throws IOException {
		response.setHeader("Content-Disposition", "attachment; filename=\"" + "kalkulator.pdf" + "\"");
		Document document = new Document();
		PdfWriter writer = null;
		try {
			writer = PdfWriter.getInstance(document, response.getOutputStream());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.open();
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, reader);
		document.close();
	}

	public static double roundTo2Dec(double value) {
		return Math.round(value * 100.0) / 100.0;
	}

}