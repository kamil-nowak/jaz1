package model;

public class Credit {
	private double creditValue;
	private int numberOfRates;
	private double interest;
	private double constantCharge;
	private String kindOfRate;

	public Credit(double creditValue, int numberOfRates, double interest, double constantCharge, String kindOfRate) {
		this.creditValue = creditValue;
		this.numberOfRates = numberOfRates;
		this.interest = interest/100;
		this.constantCharge = constantCharge;
		this.kindOfRate = kindOfRate;
	}

	public double getRate(int rateNumber) {
		double rate = 0;
		if (kindOfRate.contains("constant")){
			rate = creditValue * (interest / 12) / (1 - (1 / Math.pow(1.0 + interest / 12, numberOfRates))) + constantCharge;
		}
		if (kindOfRate.contains("decreasing")) {
			rate = creditValue / numberOfRates * (1 + (numberOfRates - rateNumber + 1) * interest / 12) + constantCharge;
		}		
		return utils.Helpers.roundTo2Dec(rate);
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public double getCreditValue() {
		return creditValue;
	}

	public void setCreditValue(double creditValue) {
		this.creditValue = utils.Helpers.roundTo2Dec(creditValue);
	}

	public int getNumberOfRates() {
		return numberOfRates;
	}

	public void setNumberOfRates(int numberOfRates) {
		this.numberOfRates = numberOfRates;
	}

	public double getConstantCharge() {
		return constantCharge;
	}

	public void setConstantCharge(double constantCharge) {
		this.constantCharge = constantCharge;
	}

	public String getKindOfRate() {
		return kindOfRate;
	}

	public void setKindOfRate(String kindOfRate) {
		this.kindOfRate = kindOfRate;
	}
}
