package model;

public class SiteSettings {
	
    private StringBuilder title;
    private StringBuilder head;
    private StringBuilder body;

    public StringBuilder getTitle() {
    	return title;
    }

    public void setTitle(String title) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<title>");
    	sb.append(title);
    	sb.append("</title>");
        this.title = sb;
    }

    public StringBuilder getHead() {
        return head;
    }

    public void setHead(String head) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<head>");
    	if (getTitle() != null){
    		sb.append(getTitle());
    	}
    	sb.append(head);
    	sb.append("</head>");
        this.head = sb;
    }
    
    public StringBuilder getBody() {
        return body;
    }

    public void setBody(String body) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<body>");
    	sb.append(body);
    	sb.append("</body>");
        this.body = sb;
    }
    
    public void appendToBody(StringBuilder stringBuilder) {
    	StringBuilder sb = getBody();
    	String sb2 = sb.toString().replace("</body>", stringBuilder);
    	StringBuilder result = new StringBuilder();
    	result.append(sb2);
    	result.append("</body>");
    	this.body = result;
    }
    
    public StringBuilder getPage() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<html>");
    	sb.append(getHead());
    	sb.append(getBody());
    	sb.append("</html>");
    	return sb;   	
    }
    
}

