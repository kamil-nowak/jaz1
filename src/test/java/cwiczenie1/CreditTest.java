package cwiczenie1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Credit;

public class CreditTest {


	@Test
	public void test_kind_of_rate_should_be_constant() {
		String kindOfRate = "constant";
		Credit credit = new Credit(5000, 10, 0.05, 10, kindOfRate);
		assertEquals(credit.getKindOfRate(), kindOfRate);
	}
	
	@Test
	public void test_rate_value() {
		Credit credit = new Credit(5000, 10, 0.05, 10, "constant");
		double rateValue = credit.getCreditValue() * (credit.getInterest() / 12) 
				/ (1 - (1 / Math.pow(1.0 + credit.getInterest() / 12, credit.getNumberOfRates())))+credit.getConstantCharge();
		double result = Math.round(rateValue * 100.0) / 100.0;
	
		double myPi = 22.0d / 7.0d; 
		assertEquals(credit.getRate(1), myPi, result);
	}

}
